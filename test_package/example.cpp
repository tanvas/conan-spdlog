#include <spdlog/spdlog.h>

int main() {
	spdlog::set_level(spdlog::level::info);	
	spdlog::info("spdlog version {}.{}.{}", SPDLOG_VER_MAJOR, SPDLOG_VER_MINOR, SPDLOG_VER_PATCH);
	spdlog::shutdown();

	return 0;
}
