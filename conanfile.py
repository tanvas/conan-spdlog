from conans import ConanFile, CMake, tools
from conans.tools import Git
import os

class SpdlogConan(ConanFile):
    name = "spdlog"
    version = "1.4.2"
    license = "MIT"
    author = "Tanvas Inc. <software@tanvas.co>"
    url = "https://bitbucket.org/tanvas/conan-spdlog"
    description = "Fast C++ logging library"
    topics = ("logging")
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "lto": [True, False],
        "fPIC": [True, False],
        "fmt_external": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": False,
        "lto": False,
        "fmt_external": True,
    }
    generators = "cmake", "cmake_find_package"

    def source(self):
        git = Git()
        git.clone("https://github.com/gabime/spdlog", shallow=True, branch="v{}".format(self.version))

        tools.replace_in_file("CMakeLists.txt",
                "project(spdlog VERSION ${SPDLOG_VERSION} LANGUAGES CXX)",
                '''
project(spdlog VERSION ${SPDLOG_VERSION} LANGUAGES CXX)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
                ''')

    def requirements(self):
        if self.options.fmt_external:
            self.requires("fmt/[~6]@bincrafters/stable")

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["SPDLOG_FMT_EXTERNAL"] = self.options.fmt_external
        cmake.configure()

        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libdirs += [os.path.join("lib", "spdlog")]

        if self.settings.build_type == "Debug":
            self.cpp_info.libs += ["spdlogd"]
        else:
            self.cpp_info.libs += ["spdlog"]

        # At least one compiler (gcc, libstdc++, arm64 Linux) requires -pthread
        # to be present when building programs including spdlog, so we just add
        # it here for all Linux builds.  (clang supports this flag also.)
        if self.settings.os == "Linux":
            self.cpp_info.cxxflags += ["-pthread"]
            self.cpp_info.exelinkflags += ["-pthread"]
            self.cpp_info.sharedlinkflags += ["-pthread"]

        self.cpp_info.defines += ["SPDLOG_COMPILED_LIB"]

        if self.options.fmt_external:
            self.cpp_info.defines += ["SPDLOG_FMT_EXTERNAL"]
